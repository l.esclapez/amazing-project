def sum(a : float, b : float) -> float:
    """A sum function of two floats."""
    return a + b
